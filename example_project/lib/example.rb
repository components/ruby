# frozen_string_literal: true

# This is an example class
class Example
  def self.example
    puts 'Goodbye!'

    return unless 1 == 2

    puts 'Not covered!'
  end
end
