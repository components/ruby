# frozen_string_literal: true

require 'pry-byebug'
require 'simplecov-cobertura'

RSpec.configure do |config|
  original_stderr = $stderr
  original_stdout = $stdout

  config.before(:all) do
    $stderr = File.open(File::NULL, 'w')
    $stdout = File.open(File::NULL, 'w')
  end

  config.after(:all) do
    $stderr = original_stderr
    $stdout = original_stdout
  end

  SimpleCov.formatters = SimpleCov::Formatter::MultiFormatter.new([SimpleCov::Formatter::CoberturaFormatter])
  SimpleCov.start do
    load_profile 'rails'
    add_filter 'vendor'
  end

  config.expect_with :rspec do |r|
    r.max_formatted_output_length = nil
  end
end
